import React, { Component } from 'react';
import {BrowserRouter,Route,Link,Switch,Redirect} from 'react-router-dom';

import logo from './logo.svg';
import './App.css';
import './toolbox/theme.css';
import {BasicInfo} from './BasicInfo.js';
import {Experience} from './Experience.js';
import {Skills} from './Skills';
import  Routes from './Routes';

import Button from 'react-toolbox/lib/button/Button';
import AppBar from 'react-toolbox/lib/app_bar/AppBar';
import Input from 'react-toolbox/lib/input/Input';
import DatePicker from 'react-toolbox/lib/date_picker/DatePicker';
import {Row,Col} from 'react-flexbox-grid/lib/index';
import Autocomplete from 'react-toolbox/lib/autocomplete';


import theme from './toolbox/theme'
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';


const source={
	'HTML5':'HTML5',
	'CSS3':'CSS3',
	'Javascript':'Javascript',
	'PHP':'PHP',
	'Java':'Java',
	'Data Structures':'Data Structures',
	'Algorithms':'Algorithms',
	'C++':'C++',
	'C':'C',
	'ReactJs':'ReactJs',
	'NodeJs':'NodeJs',
	'Python':'Python',
	'Wordpress':'Wordpress',
	'Adobe Photoshop':'Adobe Photoshop',
	'vim':'vim',
	'Linux':'Linux',
	'Git':'Git',
	'R':'R',
	'SEO':'SEO',
	'Digital Marketing':'Digital Marketing',
	'Bash':'Bash',
}

class App extends Component {

  render() {
    return (
      <ThemeProvider theme={theme}>
      <AppBar title="Resume Maker"  >


      <li><Link to={'/basicinfo'}>BasicInfo</Link></li>
      <li><Link to={'/skills'}>Skills</Link></li>
      <li><Link to={'/experience'}>Experience</Link></li>
      </AppBar>
      
      </ThemeProvider>
    );
  }
}

export default App;
