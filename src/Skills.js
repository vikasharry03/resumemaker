import React from 'react';
import { Row, Col } from 'react-flexbox-grid/lib/index';
import DatePicker from 'react-toolbox/lib/date_picker/DatePicker';
import Input from 'react-toolbox/lib/input/Input';
import Autocomplete from 'react-toolbox/lib/autocomplete';
import Button from 'react-toolbox/lib/button/Button';


export  class Skills extends React.Component {

    render() {
        return (
            <Row center="xs">
            <Col>
             <h4>Skills</h4>
                <Autocomplete
                  direction="down"
                  selectedPosition="above"
                  label="Select Skills"
                  source={this.props.source}
                  multiple={true}
                />
            <Button label="Submit" primary raised />

            </Col>
     
      </Row>


        )
    }
}