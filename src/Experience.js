import React from 'react';
import { Row, Col } from 'react-flexbox-grid/lib/index';
import DatePicker from 'react-toolbox/lib/date_picker/DatePicker';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';


export class Experience extends React.Component {

    render() {
        return (
            <Row center="xs">
      <Col xs={8}>
       <h4 style={{textAlign:'left'}}>Experience</h4>
      <div>
      <Input type="text" label="Company Name" />
      <Row>
      <Col xs={4} xsOffset={0}>
      <DatePicker label="Date Started" />
      </Col>
      
      <Col xs={4} xsOffset={2}>
      <DatePicker label="Date Completed" />
      </Col>
      </Row>
      <Input type="text" multiline label="How you Contributed there" />
      </div>
      <div>
      <Input type="text" label="Company Name" />
      <Row>
      <Col xs={4} xsOffset={0}>
      <DatePicker label="Date Started" />
      </Col>
      <Col xs={4} xsOffset={2}>
      <DatePicker label="Date Completed" />
      </Col>
      </Row>
      <Input type="text" multiline label="How you Contributed there" />
      </div>
      <Button label="Submit" primary raised />
      </Col>
      </Row>


        )
    }
}