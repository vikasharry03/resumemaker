import React from 'react';
import {BrowserRouter,Route,Link,Switch,Redirect} from 'react-router-dom';

import {BasicInfo} from './BasicInfo.js';
import {Experience} from './Experience.js';
import {Skills} from './Skills.js';



export default class Routes extends React.Component{
	render(){
		return(
			<BrowserRouter>
			   <Route exact path="/" component={BasicInfo}/>
			   <Route path="/basicinfo" component={BasicInfo}/>
			   <Route path="/skills" component={Skills}/>
			   <Route path="/experience" component={Experience}/>
            
            </BrowserRouter>




			)
	}
}