import React from 'react';
import {Row,Col} from 'react-flexbox-grid/lib/index';
import DatePicker from 'react-toolbox/lib/date_picker/DatePicker';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import AppBar from 'react-toolbox/lib/app_bar/AppBar';


export class BasicInfo extends React.Component{

	render(){
		return(
                  <div>
      
      <Input type="text" label="Name" />
      <Input type="email" label="Email Address" />
      <Input type="text" label="Website" />
      <DatePicker label="DOB" />
      <Input type="text" label="CGPA" />
      <Input type="text" label="Address" />
      <Input type="text" multiline label="About Me" />
      <Button label="Submit" primary raised />
      </div>


			)
	}
}